resource "aws_iam_group" "groupA" {
  name = "GroupA"
  path = "/lb/"
}

resource "aws_iam_user" "lb" {
  count = length(var.user_names)
  name  = var.user_names[count.index]
  path  = "/lb/"
}

resource "aws_iam_user_group_membership" "gpr_mem" {
  for_each = toset(var.user_names)
  user     = each.key
  groups = [
    aws_iam_group.groupA.name,
  ]
}